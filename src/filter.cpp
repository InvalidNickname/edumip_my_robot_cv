#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <stdio.h>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

class ImageConverter
{
  // Объявляем экземпляры NodeHandle и ImageTransport
  ros::NodeHandle nh;
  image_transport::ImageTransport it;

  // Объявляем переменные, которые будут хранить имена топиков для публикации / подписки изображений
  // Они должны будут подтягиваться из параметров launch файла
  std::string sub_topic;
  std::string grayscale_pub_topic;
  std::string filtered_pub_topic;

  // Объявляем публикаторов и подписчиков
  image_transport::Subscriber sub;
  image_transport::Publisher grayscale_pub;
  image_transport::Publisher filtered_pub;

  // Объявляем переменную, которая будет включать применение фильтра
  // Она должна будет подтягиваться из параметров launch файла
  bool is_processing_filter;
  
  // Объявляем ядро фильтра
  // Его значения должны будут подтягиваться из параметров launch файла
  std::vector<std::vector<float> > filter_kernel;


public:
  ImageConverter()
    : it (nh)
  {
    // Задаем размер 3х3 для ядра свертки фильтра
    filter_kernel.resize(3);
    for (int i=0; i<3; i++)
    {
      filter_kernel[i].resize(3);
    }

    // Подтягиваем все необходимые параметры из launch файла
    nh.getParam("image_processor/sub_topic", sub_topic);
    nh.getParam("image_processor/grayscale_pub_topic", grayscale_pub_topic);
    nh.getParam("image_processor/filtered_pub_topic", filtered_pub_topic);
    nh.getParam("image_processor/is_processing_filter", is_processing_filter);
    
    std::string tmp_filter_kernel;
    nh.getParam("image_processor/filter_kernel", tmp_filter_kernel);
    std::vector<std::string> split_filter_kernel;
    boost::split(split_filter_kernel, tmp_filter_kernel, boost::is_any_of(" "));
    for (int i = 0; i < 3; ++i)
      for (int j = 0; j < 3; ++j)
        filter_kernel[i][j] = boost::lexical_cast<float>(split_filter_kernel[3 * i + j]);
    
    filtered_pub = it.advertise(filtered_pub_topic, 1);
    grayscale_pub = it.advertise(grayscale_pub_topic, 1);
    
    // На вход из Gazebo мы получаем цветное изображение в кодировке rgb8, то есть имеем 3 канала, на каждый из которых выделяется по 8 бит
    sub = it.subscribe(sub_topic, 1, &ImageConverter::imageCallback, this);
  }

  ~ImageConverter()
  {
  }

  void imageCallback(const sensor_msgs::ImageConstPtr& msg)
  {
    // Объявляем переменные для серого и фильтрованного изображений формата sensor_msgs::Image
    sensor_msgs::Image grayscaled_image;
    sensor_msgs::Image filtered_image;

    // Задаем параметры изображений такие же, как у исходного цветного
    grayscaled_image.header = filtered_image.header = msg->header;
    grayscaled_image.width = filtered_image.width = msg->width;
    grayscaled_image.height = filtered_image.height = msg->height;
    grayscaled_image.is_bigendian = filtered_image.is_bigendian = false;
    grayscaled_image.step = filtered_image.step = msg->step ;
    grayscaled_image.data.resize(grayscaled_image.width * grayscaled_image.height);
    filtered_image.data.resize(filtered_image.width * filtered_image.height);

    // Для серого и фильтрованного изображений мы используем кодировку mono8, то есть один канал, на который выделяется 8 бит
    grayscaled_image.encoding = filtered_image.encoding = "mono8";
 
    // Небольшое вспомогательное действие, чтобы получать данные из цветного изображения в нужной кодировке
    const uint8_t* color_image_data = reinterpret_cast<const uint8_t*>(&msg->data[0]);

    // Получаем черно-белое изображение из цветного
    for (int v = 0; v < msg->height; v++)
    {
      for (int u = 0; u < msg->width; u++)
      {
        // Чтобы получить доступ к красному каналу пикселя с координатами (u,v), нужно обратиться по индексу 
        // 3*u + v*3*width, где 3 - количество каналов, в данном случае у нас их 3
        // Так как у нас цветное изображение идет в кодировке rgb8, то для каждого пикселя значение для каждого из каналов идет по порядку от красного к синему
        uint8_t red = color_image_data[3 * u + v * 3 * msg->width];
        uint8_t green = color_image_data[3 * u + v * 3 * msg->width + 1];
        uint8_t blue = color_image_data[3 * u + v * 3 * msg->width + 2];
        grayscaled_image.data[u + msg->width * v] = (red + blue + green) / 3;
        
      }
    }
    // Получаем отфильтрованное изображение из черно-белого, если значение переменной is_processing_filter стоит в true
    if (is_processing_filter)
    {
      for (int v = 0; v < grayscaled_image.height; ++v)
      {
        for (int u = 0; u < grayscaled_image.width; ++u)
        {
          float new_pixel = 0;
          for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
              int by = v + i - 1;
              int bx = u + j - 1;
              if (bx >= 0 && by >= 0) {
                new_pixel += filter_kernel[i][j]*grayscaled_image.data[bx + msg->width * by];
              }
            }
          }
          
          // В данном случае так как у нас ядро свертки фильтра имеет размер 3х3, то мы добавляем для изображения границу единочного размера со значением 0
          // Это нужно для того, чтобы разрешение отфильтрованного изображения совпадало с разрешением исходного изображения

          // Записываем для каждого пикселя его новое значение после операции свертки
          filtered_image.data[u + filtered_image.width * v] = new_pixel;
          
        }
      }
      // Публикуем в топик отфильтрованное изображение
      filtered_pub.publish(filtered_image);
    }
    // Публикуем в топик черно-белое изображение
    grayscale_pub.publish(grayscaled_image);
    
  }
};

int main(int argc, char** argv)
{
  ros::init(argc, argv, "edumip_my_robot_cv_node");
  ROS_INFO("Node for image processing was started");
  // Создаем экземпляр класса
  ImageConverter ic;
  while (ros::ok())
  {
    ros::spin();
  }
  ROS_INFO("Node for image processing was stopped");
  return 0;
}

